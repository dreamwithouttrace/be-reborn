<?php


namespace BeReborn\Gii;


use BeReborn\Console\Command;
use BeReborn\Console\Dtl;

/**
 * Class GiiService
 * @package BeReborn\Gii
 */
class GiiService extends Command
{

	public $command = 'gii:generate';

	public $description = 't=tableName db=databases m=`is generate model` c=`is generate controller`';

	/**
	 * @param Dtl $dtl
	 * @throws \Exception
	 * @return array
	 */
	public function handler(Dtl $dtl)
	{
		$databases = $dtl->get('databases', 'db');

		if (!\BeReborn::$app->has($databases)) {
			throw new \Exception('Unknown Database by ' . $databases);
		}

		return Gii::run(\BeReborn::$app->get($databases));
	}

}
