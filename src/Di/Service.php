<?php
/**
 * Created by PhpStorm.
 * User: whwyy
 * Date: 2018/4/25 0025
 * Time: 18:29
 */

namespace BeReborn\Di;


use BeReborn\Base\Component;
use BeReborn\Exception\ComponentException;
use BeReborn\Exception\NotFindClassException;
use Exception;

/**
 * Class Service
 * @package BeReborn\Di
 */
class Service extends Component
{

	private $_components = [];


	private $_definition = [];

	/**
	 * @param $id
	 *
	 * @return mixed
	 * @throws
	 */
	public function get($id)
	{
		if (!isset($this->_components[$id])) {
			if (!isset($this->_definition[$id])) {
				throw new ComponentException("Unknown component ID: $id");
			}
			$config = $this->_definition[$id];
			if (is_object($config)) {
				return $config;
			}
			$this->_components[$id] = \BeReborn::createObject($config);
		}
		$object = $this->_components[$id];
		if (method_exists($object, 'afterInit')) {
			$object->afterInit();
		}
		return $object;
	}


	/**
	 * @param $id
	 * @param $definition
	 *
	 * @throws Exception
	 */
	public function set($id, $definition)
	{
		if ($definition === NULL) {
			$this->remove($id);
			return;
		}

		unset($this->_components[$id]);

		if (is_object($definition) || is_callable($definition, TRUE)) {
			$this->_definition[$id] = $definition;
			return;
		} else if (is_array($definition)) {
			if (isset($definition['class'])) {
				$this->_definition[$id] = $definition;
			} else {
				throw new ComponentException("The configuration for the \"$id\" component must contain a \"class\" element.");
			}
		} else {
			throw new ComponentException("Unexpected configuration type for the \"$id\" component: " . gettype($definition));
		}
		$this->_components[$id] = $object = \BeReborn::createObject($definition);
	}

	/**
	 * @param $id
	 * @return bool
	 */
	public function has($id)
	{
		return isset($this->_definition[$id]) || isset($this->_components[$id]);
	}

	/**
	 * @param array $data
	 * @throws Exception
	 */
	public function setComponents(array $data)
	{
		foreach ($data as $key => $val) {
			$this->set($key, $val);
		}
	}


	/**
	 * @param $name
	 * @return mixed
	 * @throws Exception
	 */
	public function __get($name)
	{
		if ($this->has($name)) {
			return $this->get($name);
		}

		return parent::__get($name);
	}

	/**
	 * @return array
	 */
	public function lists()
	{
		return array_merge(array_keys($this->_definition), array_keys($this->_components));
	}

	/**
	 * @param $id
	 */
	public function remove($id)
	{
		unset($this->_components[$id]);
		unset($this->_definition[$id]);
	}
}
