<?php


namespace BeReborn\Canvas;

use Exception;

/**
 * Class Node
 * @package BeReborn\Canvas
 */
class Node
{

	public $x = 0;

	public $y = 0;

	public $width = 0;

	public $height = 0;

	public $source;

	public $backImage = '';

	public $mini = 'image/png';


	/**
	 * @throws Exception
	 */
	public function Draw()
	{
		if (empty($this->backImage)) {
			$this->source = imagecreatetruecolor($this->width, $this->height);
		} else {
			$this->createImageByType();
		}
		return $this->source;
	}


	/**
	 * @return resource
	 * @throws Exception
	 */
	private function createImageByType()
	{
		[$this->width, $this->height, $this->mini, $attr] = getimagesize($this->backImage);
		if ($this->mini == 'image/png') {
			$this->source = imagecreatefrompng($this->backImage);
		} else if ($this->mini == 'image/jpeg' || $this->mini == 'image/jpg') {
			$this->source = imagecreatefromjpeg($this->backImage);
		} else if ($this->mini == 'image/gif') {
			$this->source = imagecreatefromgif($this->backImage);
		} else {
			throw new Exception('暂不支持的图片类型');
		}
		return $this->source;
	}

}
