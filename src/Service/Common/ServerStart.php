<?php


namespace BeReborn\Service\Common;


use BeReborn;
use BeReborn\Base\Component;
use Swoole\Coroutine\System;
use Swoole\Server;

/**
 * Class ServerStart
 * @package BeReborn\Service\Common
 */
class ServerStart extends Component
{

	/**
	 * @param Server $server
	 */
	public function onStart($server)
	{
		$time = BeReborn::$app->runtimePath . '/socket.sock';
		file_put_contents($time, $server->master_pid);

		if (BeReborn::getPlatform()->isLinux()) {
			swoole_set_process_name(BeReborn::$app->id);
		}

		$local = BeReborn::$app->runtimePath . '/log';

		shell_exec('find ' . $local . '/ -mtime +15 -name "*.log" -exec rm -rf {} \;');
	}

}
