<?php
/**
 * Created by PhpStorm.
 * User: whwyy
 * Date: 2018/11/8 0008
 * Time: 16:44
 */

namespace BeReborn\Event;

/**
 * Interface IEvent
 * @package BeReborn\Event
 */
interface IEvent
{

	/**
	 * @param ...$params
	 * @return mixed
	 */
	public function handler(...$params);

}
