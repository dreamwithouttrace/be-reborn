<?php


namespace BeReborn\Database\Condition;

use BeReborn\Core\Str;

/**
 * Class DefaultCondition
 * @package BeReborn\Database\Condition
 */
class DefaultCondition extends Condition
{

	/**
	 * @return string
	 */
	public function builder()
	{
		return $this->resolve($this->column, $this->value, $this->opera);
	}

}
