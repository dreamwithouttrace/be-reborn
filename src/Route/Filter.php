<?php


namespace BeReborn\Route;


use BeReborn\Base\Component;
use BeReborn\Error\Logger;
use BeReborn\Exception\AuthException;
use BeReborn\Route\Filter\BodyFilter;
use BeReborn\Route\Filter\FilterException;
use BeReborn\Route\Filter\HeaderFilter;
use BeReborn\Route\Filter\QueryFilter;
use Exception;

/**
 * Class Filter
 * @package BeReborn\Route
 */
class Filter extends Component
{

	/** @var Filter\Filter[] */
	private $_filters = [];

	/** @var array */
	public $grant = [];

	/**
	 * @param array $value
	 * @return BodyFilter|bool
	 * @throws Exception
	 */
	public function setBody(array $value)
	{
		if (empty($value)) {
			return true;
		}

		/** @var BodyFilter $class */
		$class = \BeReborn::createObject(BodyFilter::class);
		$class->rules = [];
		$class->params = Input()->params();

		return $this->_filters[] = $class;
	}


	/**
	 * @param array $value
	 * @return HeaderFilter|bool
	 * @throws Exception
	 */
	public function setHeader(array $value)
	{
		if (empty($value)) {
			return true;
		}

		/** @var HeaderFilter $class */
		$class = \BeReborn::createObject(HeaderFilter::class);
		$class->rules = [];
		$class->params = request()->headers->getHeaders();

		return $this->_filters[] = $class;
	}


	/**
	 * @param array $value
	 * @return QueryFilter|bool
	 * @throws Exception
	 */
	public function setQuery(array $value)
	{
		if (empty($value)) {
			return true;
		}

		/** @var QueryFilter $class */
		$class = \BeReborn::createObject(QueryFilter::class);
		$class->rules = [];
		$class->params = request()->headers->getHeaders();

		return $this->_filters[] = $class;
	}


	/**
	 * @throws Exception
	 */
	public function handler()
	{
		if (($error = $this->filters()) !== true) {
			throw new FilterException($error);
		}
		if (!$this->grant()) {
			throw new AuthException('Authentication error.');
		}
		return true;
	}

	/**
	 * @return bool
	 */
	private function filters()
	{
		if (empty($this->_filters)) {
			return true;
		}
		foreach ($this->_filters as $filter) {
			if (!$filter->check()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @return bool|mixed
	 */
	private function grant()
	{
		if (!is_callable($this->grant, true)) {
			return true;
		}
		return call_user_func($this->grant);
	}

}
