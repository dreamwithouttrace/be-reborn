<?php


namespace BeReborn\Route;


use BeReborn\Http\Request;

/**
 * Interface IMiddleware
 * @package BeReborn\Route
 */
interface IMiddleware
{

	public function handler(Request $request,\Closure $next);

}
