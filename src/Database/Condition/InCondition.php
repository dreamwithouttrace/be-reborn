<?php


namespace BeReborn\Database\Condition;

use BeReborn\Database\ActiveQuery;

/**
 * Class InCondition
 * @package BeReborn\Database\Condition
 */
class InCondition extends Condition
{


	/**
	 * @return string
	 * @throws \Exception
	 */
	public function builder()
	{
		if ($this->value instanceof ActiveQuery) {
			$this->value = $this->value->getBuild()->getQuery($this->value);
		} else {
			$this->value = array_filter($this->format($this->value));
			if (empty($this->value)) {
				return '';
			}
			$this->value = implode(',', $this->value);
		}
		return $this->column . ' in(' . $this->value . ')';
	}

}
