<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019-03-18
 * Time: 17:22
 */

namespace BeReborn\Database\Mysql;


use BeReborn\Base\Component;
use BeReborn\Core\JSON;
use BeReborn\Database\Connection;
use BeReborn\Exception\DbException;
use Exception;

/**
 * Class Columns
 * @package BeReborn\Database\Mysql
 */
class Columns extends Component
{

	private $columns = [];

	/** @var Connection $db */
	public $db;
	public $table = '';
	private $_primary = [];
	private $_auto_increment = [];

	/**
	 * @param string $table
	 * @return $this
	 * @throws DbException
	 */
	public function table($table)
	{
		$this->table = $table;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTable()
	{
		return $this->table;
	}

	/**
	 * @param $key
	 * @param $val
	 * @return float|int|mixed|string
	 * @throws Exception
	 */
	public function fieldFormat($key, $val, $db)
	{
		return $this->encode($val, $this->get_fields($db, $key));
	}

	/**
	 * @param $data
	 * @return array
	 * @throws
	 */
	public function populate($data, $dnb)
	{
		$column = $this->get_fields($dnb);
		foreach ($data as $key => $val) {
			if (!isset($column[$key])) {
				continue;
			}
			$data[$key] = $this->decode($val, $column[$key]);
		}
		return $data;
	}

	/**
	 * @param $val
	 * @param $format
	 * @return float|int|mixed|string
	 * @throws
	 */
	public function decode($val, $format = null)
	{
		if (empty($format)) {
			return $val;
		}
		$format = strtolower($format);
		if ($this->isInt($format)) {
			return (int)$val;
		} else if ($this->isJson($format)) {
			return JSON::decode($val, true);
		} else if ($this->isFloat($format)) {
			return (float)$val;
		} else {
			return stripslashes($val);
		}
	}

	/**
	 * @param $val
	 * @param $format
	 * @return float|int|mixed|string
	 * @throws
	 */
	public function encode($val, $format = null)
	{
		if (empty($format)) {
			return $val;
		}
		$format = strtolower($format);
		if ($this->isInt($format)) {
			return (int)$val;
		} else if ($this->isJson($format)) {
			return JSON::encode($val);
		} else if ($this->isFloat($format)) {
			return (float)$val;
		} else {
			return addslashes($val);
		}
	}

	/**
	 * @param $format
	 * @return bool
	 */
	public function isInt($format)
	{
		return in_array($format, ['int', 'bigint', 'tinyint', 'smallint', 'mediumint']);
	}

	/**
	 * @param $format
	 * @return bool
	 */
	public function isFloat($format)
	{
		return in_array($format, ['float', 'double', 'decimal']);
	}

	/**
	 * @param $format
	 * @return bool
	 */
	public function isJson($format)
	{
		return in_array($format, ['json']);
	}

	/**
	 * @param $format
	 * @return bool
	 */
	public function isString($format)
	{
		return in_array($format, ['varchar', 'char', 'text', 'longtext', 'tinytext', 'mediumtext']);
	}


	/**
	 * @return array
	 * @throws
	 */
	public function format($db)
	{
		return $this->columns('Default', $db, 'Field');
	}

	/**
	 * @return int|string|null
	 * @throws Exception
	 */
	public function getAutoIncrement()
	{
		return $this->_auto_increment[$this->table] ?? null;
	}

	/**
	 * @return array|null|string
	 *
	 * @throws Exception
	 */
	public function getPrimaryKeys()
	{
		if (isset($this->_auto_increment[$this->table])) {
			return $this->_auto_increment[$this->table];
		}
		return $this->_primary[$this->table] ?? null;
	}


	/**
	 * @param $name
	 * @param null $index
	 * @return array
	 * @throws Exception
	 */
	private function columns($name, $db, $index = null): array
	{
		if (empty($index)) {
			return array_column($this->getColumns($db), $name);
		} else {
			return array_column($this->getColumns($db), $name, $index);
		}
	}

	/**
	 * @return array|bool|int|mixed|string
	 * @throws Exception
	 */
	private function getColumns($db)
	{
		return $this->structure($this->getTable(), $db);
	}


	/**
	 * @param $table
	 * @return array
	 * @throws Exception
	 */
	private function structure($table, $db): array
	{
		if (!isset($this->columns[$table]) || empty($this->columns[$table])) {
			$sql = $this->db->getBuild()->getColumn($table);
			$column = $this->db->createCommand($sql, $db)->all();
			if (empty($column)) {
				throw new Exception("The table " . $table . " not exists.");
			}
			return $this->columns[$table] = $this->resolve($column, $table);
		}
		return $this->columns[$table];
	}


	/**
	 * @param $column
	 * @param $table
	 * @return array
	 */
	private function resolve(array $column, $table): array
	{
		foreach ($column as $key => $item) {
			$column[$key]['Type'] = $this->clean($item['Type']);
		}
		return $column;
	}

	/**
	 * @param $type
	 * @return string
	 */
	private function clean($type)
	{
		if (strpos($type, ')') === false) {
			return $type;
		}

		$replace = preg_replace('/\(\d+(,\d+)?\)(\s+\w+)*/', '', $type);
		if (strpos(' ', $replace) !== FALSE) {
			$replace = explode(' ', $replace)[1];
		}
		return $replace;
	}

	/**
	 * @param $field
	 * @return array|string
	 * @throws Exception
	 */
	public function get_fields($db, $field = null)
	{
		$fields = $this->columns('Type', $db, 'Field');
		if (empty($field)) {
			return $fields;
		}
		if (!isset($fields[$field])) {
			return null;
		}
		return strtolower($fields[$field]);
	}

}
