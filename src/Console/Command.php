<?php


namespace BeReborn\Console;

use BeReborn\Base\BaseObject;

/**
 * Class Command
 * @package BeReborn\Console
 */
abstract class Command extends BaseObject implements CommandInterface
{

	public $command = '';
	public $description = '';

	/**
	 * @return string
	 * 返回执行的命令名称
	 */
	public function getName()
	{
		return $this->command;
	}


	/**
	 * @return string
	 *
	 * 返回命令描述
	 */
	public function getDescription()
	{
		return $this->description;
	}

}
