<?php
declare(strict_types=1);

namespace kafka\Exception;

use kafka\Exception;

class NotSupported extends Exception
{
}
