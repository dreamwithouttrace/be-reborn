<?php


namespace BeReborn\Base;


/**
 * Class Platform
 * @package BeReborn\Base
 */
class Platform
{

	/**
	 * @return bool
	 */
	public function isMac()
	{
		$output = strtolower(PHP_OS | PHP_OS_FAMILY);
		if (strpos('mac', $output) !== false) {
			return true;
		} else if (strpos('darwin', $output) !== false) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return bool
	 */
	public function isLinux()
	{
		if (!$this->isMac()) {
			return true;
		} else {
			return false;
		}
	}

}
