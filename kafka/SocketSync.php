<?php
declare(strict_types=1);

namespace kafka;

use Swoole\Coroutine\Client;
use function fclose;
use function is_resource;
use function rewind;
use function stream_set_blocking;

class SocketSync extends CommonSocket
{
    public function connect(): void
    {
        if ($this->stream instanceof Client) {
            return;
        }

	    try {
		    $this->createStream();
	    } catch (\Swoole\Exception $e) {
	    }

//        stream_set_blocking($this->stream, false);
    }

    public function close(): void
    {
        if ($this->stream instanceof Client) {
	        $this->stream->close();
        }
    }

    public function isResource(): bool
    {
        return is_resource($this->stream);
    }

    /**
     * @param string|int $data
     *
     * @throws  \kafka\Exception
     */
    public function read($data)
    {
        return $this->readBlocking((int) $data);
    }

    /**
     * @throws  \kafka\Exception
     */
    public function write(?string $buffer = null)
    {
        if ($buffer === null) {
            throw new Exception('You must inform some data to be written');
        }

        return $this->writeBlocking($buffer);
    }

    public function rewind(): void
    {
        if (is_resource($this->stream)) {
            rewind($this->stream);
        }
    }
}
