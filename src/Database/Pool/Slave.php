<?php


namespace BeReborn\Database\Pool;

/**
 * Class Slave
 * @package BeReborn\Database\Pool
 */
class Slave extends DbConnection
{
	public $host = '';
	public $username = '';
	public $password = '';


	public $slaveConfig = [];

	/**
	 * @return mixed|void
	 */
	public function getConnectConfig()
	{
		$config = $this->slaveConfig;
		$config[] = [
			'cds' => $this->host,
			'username' => $this->username,
			'password' => $this->password
		];
		return $config[array_rand($config)];
	}

}
