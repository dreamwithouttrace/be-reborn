<?php
/**
 * Created by PhpStorm.
 * User: whwyy
 * Date: 2018/10/7 0007
 * Time: 2:16
 */

namespace BeReborn\Console;


use BeReborn\Base\BaseApplication;
use kafka\Protocol\SyncGroup;
use Swoole\Coroutine\Channel;
use Swoole\Runtime;
use Swoole\Timer;

/**
 * Class Application
 * @package BeReborn\Console
 */
class Application extends BaseApplication
{

	/**
	 * @var string
	 */
	public $id = 'uniqueId';

	private $console;

	/** @var Channel */
	private $channel;

	/**
	 * Application constructor.
	 * @param array $config
	 * @throws
	 */
	public function __construct(array $config = [])
	{
		parent::__construct($config);

		$this->channel = new Channel(1);
		$this->console = new Console($config);
	}

	/**
	 * @param $class
	 * @throws
	 */
	public function register($class)
	{
		if (is_string($class) || is_callable($class, true)) {
			$class = \BeReborn::createObject($class);
		}
		$this->console->signCommand($class);
	}


	/**
	 * @param null $kernel
	 * @return string|void
	 * @throws \Exception
	 */
	public function run($kernel = null)
	{
		setCommand(true);
		try {
			$params = '';
			$kernel = \BeReborn::make($kernel, Kernel::class);

			Runtime::enableCoroutine(true, SWOOLE_HOOK_TCP |
				SWOOLE_HOOK_UNIX |
				SWOOLE_HOOK_UDP |
				SWOOLE_HOOK_UDG |
				SWOOLE_HOOK_SSL |
				SWOOLE_HOOK_TLS |
				SWOOLE_HOOK_SLEEP |
				SWOOLE_HOOK_STREAM_FUNCTION |
				SWOOLE_HOOK_PROC
			);

            fire('SERVER_BEFORE_WORKER');

			$this->console->setParameters();
			$this->console->batch($kernel);
			$class = $this->console->search();
			$params = response()->send($this->console->execCommand($class));
		} catch (\Exception $exception) {
			$params = response()->send(implode("\n", [
				'Msg: ' . $exception->getMessage(),
				'Line: ' . $exception->getLine(),
				'File: ' . $exception->getFile()
			]));
		} finally {
			Timer::clearAll();
			return $params;
		}
	}

}
