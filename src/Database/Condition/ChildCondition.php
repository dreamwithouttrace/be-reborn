<?php


namespace BeReborn\Database\Condition;

use BeReborn\Core\Str;

/**
 * Class ChildCondition
 * @package BeReborn\Database\Condition
 */
class ChildCondition extends Condition
{

	/**
	 * @return string
	 */
	public function builder()
	{
		return $this->column . ' ' . $this->opera . ' (' . $this->value . ')';
	}

}
