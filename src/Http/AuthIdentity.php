<?php


namespace BeReborn\Http;


/**
 * Interface AuthIdentity
 * @package BeReborn\Http
 */
interface AuthIdentity
{



	public function getIdentity();

}
