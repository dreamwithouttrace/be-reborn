<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019-03-20
 * Time: 10:40
 */

namespace BeReborn\Exception;

/**
 * Class ExitException
 * @package BeReborn\Exception
 */
class ExitException extends \Exception
{

}
