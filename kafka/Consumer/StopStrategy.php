<?php
declare(strict_types=1);

namespace kafka\Consumer;

use kafka\Consumer;

interface StopStrategy
{
    public function setup(Consumer $consumer): void;
}
