<?php


namespace BeReborn\Database\Base;


use BeReborn\Database\Condition\BetweenCondition;
use BeReborn\Database\Condition\InCondition;
use BeReborn\Database\Condition\LikeCondition;
use BeReborn\Database\Condition\LLikeCondition;
use BeReborn\Database\Condition\MathematicsCondition;
use BeReborn\Database\Condition\NotBetweenCondition;
use BeReborn\Database\Condition\NotInCondition;
use BeReborn\Database\Condition\NotLikeCondition;
use BeReborn\Database\Condition\RLikeCondition;

/**
 * Class ConditionClassMap
 * @package BeReborn\Database\Base
 */
class ConditionClassMap
{

	/** @var array */
	public static $conditionMap = [
		'IN'          => [
			'class' => InCondition::class
		],
		'NOT IN'      => [
			'class' => NotInCondition::class
		],
		'LIKE'        => [
			'class' => LikeCondition::class
		],
		'NOT LIKE'    => [
			'class' => NotLikeCondition::class
		],
		'LLike'       => [
			'class' => LLikeCondition::class
		],
		'RLike'       => [
			'class' => RLikeCondition::class
		],
		'EQ'          => [
			'class' => MathematicsCondition::class,
			'type'  => 'EQ'
		],
		'NEQ'         => [
			'class' => MathematicsCondition::class,
			'type'  => 'NEQ'
		],
		'GT'          => [
			'class' => MathematicsCondition::class,
			'type'  => 'GT'
		],
		'EGT'         => [
			'class' => MathematicsCondition::class,
			'type'  => 'EGT'
		],
		'LT'          => [
			'class' => MathematicsCondition::class,
			'type'  => 'LT'
		],
		'ELT'         => [
			'class' => MathematicsCondition::class,
			'type'  => 'ELT'
		],
		'BETWEEN'     => BetweenCondition::class,
		'NOT BETWEEN' => NotBetweenCondition::class,
	];

}
