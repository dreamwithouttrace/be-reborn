<?php


namespace BeReborn\Annotation\Base;


use BeReborn\Base\Component;

/**
 * Class BaseAnnotation
 * @package BeReborn\Annotation\Base
 */
abstract class BaseAnnotation extends Component
{

	public function each()
	{

	}

}
