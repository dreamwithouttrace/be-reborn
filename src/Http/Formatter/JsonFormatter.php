<?php
/**
 * Created by PhpStorm.
 * User: whwyy
 * Date: 2018/4/8 0008
 * Time: 17:18
 */

namespace BeReborn\Http\Formatter;


use BeReborn\Base\Component;
use BeReborn\Core\ArrayAccess;
use BeReborn\Core\JSON;
use BeReborn\Database\ActiveRecord;
use BeReborn\Database\Collection;
use Exception;

/**
 * Class JsonFormatter
 * @package BeReborn\Http\Formatter
 */
class JsonFormatter extends Component implements IFormatter
{
	public $data;

	public $status = 200;

	public $header = [];

	/**
	 * @param $data
	 * @return $this|IFormatter
	 * @throws Exception
	 */
	public function send($data)
	{
		if (!is_string($data)) {
			$data = json_encode($data);
		}
		$this->data = $data;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getData()
	{
		$data = $this->data;
		$this->clear();
		return $data;
	}


	public function clear()
	{
		unset($this->data);
	}
}
