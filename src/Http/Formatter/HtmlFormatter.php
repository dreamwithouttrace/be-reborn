<?php
/**
 * Created by PhpStorm.
 * User: whwyy
 * Date: 2018/4/8 0008
 * Time: 17:51
 */

namespace BeReborn\Http\Formatter;


use BeReborn\Base\Component;
use BeReborn\Core\ArrayAccess;
use BeReborn\Core\JSON;
use Swoole\Http\Response;

/**
 * Class HtmlFormatter
 * @package BeReborn\Http\Formatter
 */
class HtmlFormatter extends Component implements IFormatter
{

	public $data;

	/** @var Response */
	public $status;

	public $header = [];

	/**
	 * @param $data
	 * @return $this
	 * @throws \Exception
	 */
	public function send($data)
	{
		if (!is_string($data)) {
			$data = JSON::encode($data);
		}
		$this->data = $data;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getData()
	{
		$data = $this->data;
		$this->clear();
		return $data;
	}

	public function clear()
	{
		unset($this->data);
	}
}
