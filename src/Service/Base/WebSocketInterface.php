<?php


namespace BeReborn\Service\Base;


use Swoole\WebSocket\Frame;
use Swoole\WebSocket\Server;

/**
 * Interface WebSocketInterface
 * @package BeReborn\Service\Base
 */
interface WebSocketInterface
{

	/**
	 * @param Server $server
	 * @param Frame $frame
	 * @return mixed
	 */
	public function onMessage(Server $server, Frame $frame);


	/**
	 * @param Server $server
	 * @param int $fd
	 * @return mixed
	 */
	public function onClose(Server $server, int $fd);

}
