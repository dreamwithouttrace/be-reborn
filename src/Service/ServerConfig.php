<?php


namespace BeReborn\Service;


use Exception;

class ServerConfig
{

    public $worker_num = 6;
    public $reactor_num = 4;
    public $backlog = null;
    public $max_request = null;
    public $open_eof_check = FALSE;
    public $http_parse_post = TRUE;
    public $package_eof = "\r\n\r\n";
    public $dispatch_mode = 1;
    public $daemonize = 1;
    public $open_cpu_affinity = 1;
    public $open_tcp_keepalive = 1;
    public $open_tcp_nodelay = 1;
    public $tcp_keepinterval = 0.2; //1s探测一次
    public $tcp_keepcount = 3;
    public $tcp_defer_accept = 1;
    public $task_worker_num = null;
    public $discard_timeout_request = FALSE;
    public $open_mqtt_protocol = TRUE;
    public $task_ipc_mode = 1;
    public $pid_file = APP_PATH . '/runtime/socket.sock';
    public $log_file = APP_PATH . '/runtime/log_file.log';
    public $task_max_request = null;
    public $task_enable_coroutine = TRUE;
    public $message_queue_key = 0x72000120;
    public $tcp_fastopen = TRUE;
    public $reload_async = TRUE;
    public $heartbeat_idle_time = 600;
    public $heartbeat_check_interval = 50;
    public $max_coroutine = null;
    public $package_max_length = 3096000;
    public $open_websocket_close_frame = TRUE;
    public $websocket_subprotocol = TRUE;
    public $open_http_protocol = TRUE;
    public $enable_coroutine = TRUE;


    /**
     * @return float
     */
    public function getTcpKeepinterval(): float
    {
        return $this->tcp_keepinterval;
    }

    /**
     * @param float $tcp_keepinterval
     */
    public function setTcpKeepinterval(float $tcp_keepinterval): void
    {
        $this->tcp_keepinterval = $tcp_keepinterval;
    }

    /**
     * @return int
     */
    public function getTcpKeepcount()
    {
        return $this->tcp_keepcount;
    }

    /**
     * @param int $tcp_keepcount
     */
    public function setTcpKeepcount(int $tcp_keepcount): void
    {
        $this->tcp_keepcount = $tcp_keepcount;
    } //探测的次数，超过5次后还没回包close此连接

    /**
     * @return int
     */
    public function getMaxCoroutine()
    {
        return $this->max_coroutine;
    }

    /**
     * @param int $max_coroutine
     */
    public function setMaxCoroutine(int $max_coroutine): void
    {
        $this->max_coroutine = $max_coroutine;
    }

    /**
     * @return string
     */
    public function getLogFile()
    {
        return $this->log_file;
    }

    /**
     * @param string $log_file
     */
    public function setLogFile(string $log_file): void
    {
        $this->log_file = $log_file;
    }

    /**
     * @return int
     */
    public function getOpenTcpKeepalive()
    {
        return $this->open_tcp_keepalive;
    }

    /**
     * @param int $open_tcp_keepalive
     */
    public function setOpenTcpKeepalive(int $open_tcp_keepalive): void
    {
        $this->open_tcp_keepalive = $open_tcp_keepalive;
    }

    /**
     * @return static
     * @throws Exception
     */
    public static function getInstance()
    {
        return \BeReborn::createObject(get_called_class());
    }

    /**
     * @return string
     */
    public function getPidFile()
    {
        return $this->pid_file;
    }

    /**
     * @param string $pid_file
     */
    public function setPidFile(string $pid_file): void
    {
        $this->pid_file = $pid_file;
    }

    /**
     * @return int
     */
    public function getWorkerNum()
    {
        return $this->worker_num;
    }

    /**
     * @param int $worker_num
     */
    public function setWorkerNum(int $worker_num): void
    {
        $this->worker_num = $worker_num;
    }

    /**
     * @return int
     */
    public function getReactorNum()
    {
        return $this->reactor_num;
    }

    /**
     * @param int $reactor_num
     */
    public function setReactorNum(int $reactor_num): void
    {
        $this->reactor_num = $reactor_num;
    }

    /**
     * @return int
     */
    public function getBacklog()
    {
        return $this->backlog;
    }

    /**
     * @param int $backlog
     */
    public function setBacklog(int $backlog): void
    {
        $this->backlog = $backlog;
    }

    /**
     * @return int
     */
    public function getMaxRequest()
    {
        return $this->max_request;
    }

    /**
     * @param int $max_request
     */
    public function setMaxRequest(int $max_request): void
    {
        $this->max_request = $max_request;
    }


    /**
     * @return bool
     */
    public function isOpenEofCheck(): bool
    {
        return $this->open_eof_check;
    }

    /**
     * @param bool $open_eof_check
     */
    public function setOpenEofCheck(bool $open_eof_check): void
    {
        $this->open_eof_check = $open_eof_check;
    }

    /**
     * @return bool
     */
    public function isHttpParsePost(): bool
    {
        return $this->http_parse_post;
    }

    /**
     * @param bool $http_parse_post
     */
    public function setHttpParsePost(bool $http_parse_post): void
    {
        $this->http_parse_post = $http_parse_post;
    }

    /**
     * @return string
     */
    public function getPackageEof()
    {
        return $this->package_eof;
    }

    /**
     * @param string $package_eof
     */
    public function setPackageEof(string $package_eof): void
    {
        $this->package_eof = $package_eof;
    }

    /**
     * @return int
     */
    public function getDispatchMode()
    {
        return $this->dispatch_mode;
    }

    /**
     * @param int $dispatch_mode
     */
    public function setDispatchMode(int $dispatch_mode): void
    {
        $this->dispatch_mode = $dispatch_mode;
    }

    /**
     * @return int
     */
    public function getDaemonize()
    {
        return $this->daemonize;
    }

    /**
     * @param int $daemonize
     */
    public function setDaemonize(int $daemonize): void
    {
        $this->daemonize = $daemonize;
    }

    /**
     * @return int
     */
    public function getOpenCpuAffinity()
    {
        return $this->open_cpu_affinity;
    }

    /**
     * @param int $open_cpu_affinity
     */
    public function setOpenCpuAffinity(int $open_cpu_affinity): void
    {
        $this->open_cpu_affinity = $open_cpu_affinity;
    }

    /**
     * @return int
     */
    public function getOpenTcpNodelay()
    {
        return $this->open_tcp_nodelay;
    }

    /**
     * @param int $open_tcp_nodelay
     */
    public function setOpenTcpNodelay(int $open_tcp_nodelay): void
    {
        $this->open_tcp_nodelay = $open_tcp_nodelay;
    }

    /**
     * @return int
     */
    public function getTcpDeferAccept()
    {
        return $this->tcp_defer_accept;
    }

    /**
     * @param int $tcp_defer_accept
     */
    public function setTcpDeferAccept(int $tcp_defer_accept): void
    {
        $this->tcp_defer_accept = $tcp_defer_accept;
    }

    /**
     * @return int
     */
    public function getTaskWorkerNum()
    {
        return $this->task_worker_num;
    }

    /**
     * @param int $task_worker_num
     */
    public function setTaskWorkerNum(int $task_worker_num): void
    {
        $this->task_worker_num = $task_worker_num;
    }

    /**
     * @return bool
     */
    public function isDiscardTimeoutRequest(): bool
    {
        return $this->discard_timeout_request;
    }

    /**
     * @param bool $discard_timeout_request
     */
    public function setDiscardTimeoutRequest(bool $discard_timeout_request): void
    {
        $this->discard_timeout_request = $discard_timeout_request;
    }

    /**
     * @return bool
     */
    public function isOpenMqttProtocol(): bool
    {
        return $this->open_mqtt_protocol;
    }

    /**
     * @param bool $open_mqtt_protocol
     */
    public function setOpenMqttProtocol(bool $open_mqtt_protocol): void
    {
        $this->open_mqtt_protocol = $open_mqtt_protocol;
    }

    /**
     * @return int
     */
    public function getTaskIpcMode()
    {
        return $this->task_ipc_mode;
    }

    /**
     * @param int $task_ipc_mode
     */
    public function setTaskIpcMode(int $task_ipc_mode): void
    {
        $this->task_ipc_mode = $task_ipc_mode;
    }

    /**
     * @return int
     */
    public function getTaskMaxRequest()
    {
        return $this->task_max_request;
    }

    /**
     * @param int $task_max_request
     */
    public function setTaskMaxRequest(int $task_max_request): void
    {
        $this->task_max_request = $task_max_request;
    }

    /**
     * @return bool
     */
    public function isTaskEnableCoroutine(): bool
    {
        return $this->task_enable_coroutine;
    }

    /**
     * @param bool $task_enable_coroutine
     */
    public function setTaskEnableCoroutine(bool $task_enable_coroutine): void
    {
        $this->task_enable_coroutine = $task_enable_coroutine;
    }

    /**
     * @return int
     */
    public function getMessageQueueKey()
    {
        return $this->message_queue_key;
    }

    /**
     * @param int $message_queue_key
     */
    public function setMessageQueueKey(int $message_queue_key): void
    {
        $this->message_queue_key = $message_queue_key;
    }

    /**
     * @return bool
     */
    public function isTcpFastopen(): bool
    {
        return $this->tcp_fastopen;
    }

    /**
     * @param bool $tcp_fastopen
     */
    public function setTcpFastopen(bool $tcp_fastopen): void
    {
        $this->tcp_fastopen = $tcp_fastopen;
    }

    /**
     * @return bool
     */
    public function isReloadAsync(): bool
    {
        return $this->reload_async;
    }

    /**
     * @param bool $reload_async
     */
    public function setReloadAsync(bool $reload_async): void
    {
        $this->reload_async = $reload_async;
    }

    /**
     * @return int
     */
    public function getHeartbeatIdleTime()
    {
        return $this->heartbeat_idle_time;
    }

    /**
     * @param int $heartbeat_idle_time
     */
    public function setHeartbeatIdleTime(int $heartbeat_idle_time): void
    {
        $this->heartbeat_idle_time = $heartbeat_idle_time;
    }

    /**
     * @return int
     */
    public function getHeartbeatCheckInterval()
    {
        return $this->heartbeat_check_interval;
    }

    /**
     * @param int $heartbeat_check_interval
     */
    public function setHeartbeatCheckInterval(int $heartbeat_check_interval): void
    {
        $this->heartbeat_check_interval = $heartbeat_check_interval;
    }

    /**
     * @return int
     */
    public function getPackageMaxLength()
    {
        return $this->package_max_length;
    }

    /**
     * @param int $package_max_length
     */
    public function setPackageMaxLength(int $package_max_length): void
    {
        $this->package_max_length = $package_max_length;
    }

    /**
     * @return bool
     */
    public function isOpenWebsocketCloseFrame(): bool
    {
        return $this->open_websocket_close_frame;
    }

    /**
     * @param bool $open_websocket_close_frame
     */
    public function setOpenWebsocketCloseFrame(bool $open_websocket_close_frame): void
    {
        $this->open_websocket_close_frame = $open_websocket_close_frame;
    }

    /**
     * @return bool
     */
    public function isWebsocketSubprotocol(): bool
    {
        return $this->websocket_subprotocol;
    }

    /**
     * @param bool $websocket_subprotocol
     */
    public function setWebsocketSubprotocol(bool $websocket_subprotocol): void
    {
        $this->websocket_subprotocol = $websocket_subprotocol;
    }

    /**
     * @return bool
     */
    public function isWebsocketCompression(): bool
    {
        return $this->websocket_compression;
    }

    /**
     * @param bool $websocket_compression
     */
    public function setWebsocketCompression(bool $websocket_compression): void
    {
        $this->websocket_compression = $websocket_compression;
    }

    /**
     * @return bool
     */
    public function isOpenHttpProtocol(): bool
    {
        return $this->open_http_protocol;
    }

    /**
     * @param bool $open_http_protocol
     */
    public function setOpenHttpProtocol(bool $open_http_protocol): void
    {
        $this->open_http_protocol = $open_http_protocol;
    }

    /**
     * @return bool
     */
    public function isEnableCoroutine(): bool
    {
        return $this->enable_coroutine;
    }

    /**
     * @param bool $enable_coroutine
     */
    public function setEnableCoroutine(bool $enable_coroutine): void
    {
        $this->enable_coroutine = $enable_coroutine;
    }

	/**
	 * @return array
	 * @throws Exception
	 */
    public static function toArray()
    {
        $array = get_object_vars(static::getInstance());
        return array_filter($array, function ($value) {
            return $value !== null;
        });
    }
}
