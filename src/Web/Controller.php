<?php
/**
 * Created by PhpStorm.
 * User: whwyy
 * Date: 2018/4/24 0024
 * Time: 19:28
 */

namespace BeReborn\Web;


use BeReborn\Base\Component;
use BeReborn\Http\HttpHeaders;
use BeReborn\Http\HttpParams;
use BeReborn\Http\Request;
use Exception;

/**
 * Class Controller
 * @package BeReborn\Web
 */
abstract class Controller extends Component
{

	/** @var HttpParams $input */
	protected $input;


	/** @var HttpHeaders */
	protected $headers;


	/** @var Request */
	protected $request;

	/**
	 * @param HttpParams $input
	 */
	public function setInput(HttpParams $input): void
	{
		$this->input = $input;
	}

	/**
	 * @param HttpHeaders $headers
	 */
	public function setHeaders(HttpHeaders $headers): void
	{
		$this->headers = $headers;
	}

	/**
	 * @param Request $request
	 */
	public function setRequest(Request $request): void
	{
		$this->request = $request;
	}

	/**
	 * @return HttpParams
	 * @throws Exception
	 */
	public function getInput(): HttpParams
	{
		if (!$this->input) {
			$this->input = $this->getRequest()->params;
		}
		return $this->input;
	}

	/**
	 * @return HttpHeaders
	 * @throws Exception
	 */
	public function getHeaders(): HttpHeaders
	{
		if (!$this->headers) {
			$this->headers = $this->getRequest()->headers;
		}
		return $this->headers;
	}

	/**
	 * @return Request
	 * @throws Exception
	 */
	public function getRequest(): Request
	{
		if (!$this->request) {
			$this->request = \BeReborn::$app->getRequest();
		}
		return $this->request;
	}

	/**
	 * @param $name
	 * @return mixed|null
	 * @throws Exception
	 */
	public function __get($name)
	{
		$method = 'get' . ucfirst($name);
		if (method_exists($this, $method)) {
			return $this->$method();
		}
		return parent::__get($name);
	}

}
