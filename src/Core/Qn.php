<?php
/**
 * Created by PhpStorm.
 * User: whwyy
 * Date: 2018/4/27 0027
 * Time: 14:02
 */

namespace BeReborn\Core;


use Qiniu\Auth;
use Qiniu\Config;
use Qiniu\Storage\BucketManager;
use Qiniu\Storage\UploadManager;
use BeReborn\Base\Component;


/**
 * Class Qn
 * @package BeReborn\Core
 *
 * @method static uploader($tmpFile, $newName)
 * @method static exists($hashKey)
 * @method static remove($hashKey)
 * @method static getState($hashKey)
 * @method static batchRemove(array $hashKeys)
 */
class Qn extends Component
{

	public $access_key;

	public $secret_key;

	public $bucket;

	/** @var Auth */
	private $auth;

	/** @var string $token */
	private $token;

	/**
	 * @return string
	 */
	private function runGetToken()
	{
		return $this->getToken();
	}

	/**
	 * @return mixed
	 */
	public function getToken()
	{
		if (!empty($this->token)) {
			return $this->token;
		}
		if (!$this->auth) {
			$this->auth = new Auth($this->access_key, $this->secret_key);
		}
		return $this->token = $this->auth->uploadToken($this->bucket);
	}

	/**
	 * @param $tmpFile
	 * @param $newName
	 * @return array
	 * @throws
	 */
	private function runUploader($tmpFile, $newName)
	{
		$uploadMgr = new UploadManager();
		return $uploadMgr->putFile($this->getToken(), $newName, $tmpFile);
	}

	/**
	 * @param $key
	 * @return bool
	 */
	private function runExists($key)
	{
		$bucketMgr = new BucketManager($this->auth);
		list($ret, $err) = $bucketMgr->stat($this->bucket, $key);
		if ($err === NULL) {
			return TRUE;
		} else {
			return FALSE;
		}
	}


	/**
	 * @param $hashKey
	 * @return bool
	 */
	private function runRemove($hashKey)
	{
		$bucketManager = new BucketManager($this->auth);
		$err = $bucketManager->delete($this->bucket, $hashKey);
		if ($err) {
			return false;
		}
		return true;
	}

	/**
	 * @param $hashKey
	 * @return bool
	 * 获取文件信息
	 */
	private function runGetState($hashKey)
	{
		$bucketManager = new BucketManager($this->auth);
		list($fileInfo, $err) = $bucketManager->stat($this->bucket, $hashKey);
		if ($err) {
			return false;
		}
		return $fileInfo;
	}

	/**
	 * @param array $hashKeys
	 * @return bool
	 * 批量删除
	 */
	private function runBatchRemove(array $hashKeys)
	{
		$bucketManager = new BucketManager($this->auth);
		$ops = $bucketManager->buildBatchDelete($this->bucket, $hashKeys);
		list($fileInfo, $err) = $bucketManager->batch($ops);
		if ($err) {
			return false;
		}
		return $fileInfo;
	}

	/**
	 * @param $name
	 * @param $arguments
	 * @return mixed
	 * @throws
	 */
	public static function __callStatic($name, $arguments)
	{
		$service = \BeReborn::getApp('qn');

		return $service->{'run' . ucfirst($name)}(...$arguments);
	}
}
