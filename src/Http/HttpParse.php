<?php


namespace BeReborn\Http;

/**
 * Class HttpParse
 * @package BeReborn\Http
 */
class HttpParse
{
	/**
	 * @param ...$object
	 * @return string
	 */
	private static function getKey(...$object)
	{
		$first = '';
		$tp = [];
		foreach ($object as $key => $value) {
			if ($value === null) {
				continue;
			}
			if (is_array($value)) {
				$value = key($value);
			}
			if ($first === '') {
				$first = $value;
			} else {
				$tp[] = $value;
			}
		}
		$key = $first . '[' . implode('][', $tp) . ']';
		if (count($tp) < 1) {
			$key = $first;
		}
		return $key;
	}

	/**
	 * @param $data
	 * @return string
	 */
	public static function parse($data)
	{
		$tmp = [];
		if (is_string($data)) {
			return $data;
		}
		foreach ($data as $key => $datum) {
			if ($datum === null) {
				continue;
			}
			$tmp[] = static::ifElse($key, $datum);
		}
		return implode('&', $tmp);
	}

	/**
	 * @param $t
	 * @param $qt
	 * @return string
	 */
	private static function ifElse($t, $qt)
	{
		if (is_string($qt) || is_numeric($qt)) {
			$string = $t . '=' . urlencode($qt);
		} else {
			$string = static::encode($t, $qt);
		}
		return $string;
	}

	/**
	 * @param ...$object
	 * @return string
	 */
	private static function encode(...$object)
	{
		$ret = [];

		$data = $object[count($object) - 1];
		$key = static::getKey(...$object);
		foreach ($data as $s => $datum) {
			if (is_array($datum)) {
				$object[count($object) - 1] = $s;
				$object[] = $datum;
				$string = static::encode(...$object);
			} else {
				$string = $key . '=' . urlencode($datum);
			}
			$ret[] = $string;
		}
		return implode('&', $ret);
	}


}
