<?php


namespace BeReborn\Canvas;

use Exception;

/**
 * Class AbstractCanvas
 * @package BeReborn\Canvas
 */
abstract class AbstractCanvas
{

	private $_parent;

	private $_childes = [];

	private $_index = 0;

	private $_x = 0;

	private $_y = 0;

	private $_width = 0;

	private $_height = 0;

	private $_source;

	private $_backImage = '';

	private $_mini = 'image/png';

	/**
	 * @return mixed
	 */
	public function getParent()
	{
		return $this->_parent;
	}

	/**
	 * @param $parent
	 */
	public function setParent($parent): void
	{
		$this->_parent = $parent;
	}

	/**
	 * @return array
	 */
	public function getChildes(): array
	{
		return $this->_childes;
	}

	/**
	 * @return int
	 */
	public function getIndex()
	{
		return $this->_index;
	}

	/**
	 * @param int $index
	 */
	public function setIndex(int $index): void
	{
		$this->_index = $index;
	}

	/**
	 * @return int
	 */
	public function getX()
	{
		return $this->_x;
	}

	/**
	 * @param int $x
	 */
	public function setX(int $x): void
	{
		$this->_x = $x;
	}

	/**
	 * @return int
	 */
	public function getY()
	{
		return $this->_y;
	}

	/**
	 * @param int $y
	 */
	public function setY(int $y): void
	{
		$this->_y = $y;
	}

	/**
	 * @return int
	 */
	public function getWidth()
	{
		return $this->_width;
	}

	/**
	 * @param int $width
	 */
	public function setWidth(int $width): void
	{
		$this->_width = $width;
	}

	/**
	 * @return int
	 */
	public function getHeight()
	{
		return $this->_height;
	}

	/**
	 * @param int $height
	 */
	public function setHeight(int $height): void
	{
		$this->_height = $height;
	}

	/**
	 * @return mixed
	 */
	public function getSource()
	{
		return $this->_source;
	}

	/**
	 * @param $source
	 */
	public function setSource($source): void
	{
		$this->_source = $source;
	}

	/**
	 * @return string
	 */
	public function getBackImage()
	{
		return $this->_backImage;
	}

	/**
	 * @param string $backImage
	 */
	public function setBackImage(string $backImage): void
	{
		$this->_backImage = $backImage;
	}

	/**
	 * @return string
	 */
	public function getMini()
	{
		return $this->_mini;
	}

	/**
	 * @param string $mini
	 */
	public function setMini(string $mini): void
	{
		$this->_mini = $mini;
	}


	/**
	 * @throws Exception
	 */
	public function instance()
	{
		if (empty($this->backImage)) {
			$this->setSource(imagecreatetruecolor($this->getWidth(), $this->getHeight()));
		} else {
			$this->createImageByType();
		}
		return $this->getSource();
	}

	/**
	 * @param $r
	 * @param $g
	 * @param $b
	 */
	public function DrawBackgroundColor($r, $g, $b)
	{
		imagecolorallocate($this->getSource(), $r, $g, $b);
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	private function createImageByType()
	{
		[$width, $height, $mini, $attr] = getimagesize($this->getBackImage());
		$this->setMini($mini);
		$this->setWidth($width);
		$this->setHeight($height);
		if ($this->getMini() == 'image/png') {
			$this->setSource(imagecreatefrompng($this->getBackImage()));
		} else if ($this->getMini() == 'image/jpeg' || $this->getMini() == 'image/jpg') {
			$this->setSource(imagecreatefromjpeg($this->getBackImage()));
		} else if ($this->getMini() == 'image/gif') {
			$this->setSource(imagecreatefromgif($this->getBackImage()));
		} else {
			throw new Exception('暂不支持的图片类型');
		}
		return $this->getBackImage();
	}

	/**
	 * @param Canvas $canvas
	 */
	public function addChild(Canvas $canvas)
	{
		$this->_childes[] = $canvas;
	}

	/**
	 * @return array
	 */
	public function getChild()
	{
		return $this->_childes;
	}

}
