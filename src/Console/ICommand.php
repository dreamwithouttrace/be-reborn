<?php


namespace BeReborn\Console;


interface ICommand
{

	public function handler();

}
