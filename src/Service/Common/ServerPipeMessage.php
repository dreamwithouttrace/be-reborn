<?php


namespace BeReborn\Service\Common;

use BeReborn;
use Swoole\Server;
use BeReborn\Service\Base\ServerBase;

/**
 * Class ServerMessage
 * @package Yoc\server
 */
class ServerPipeMessage extends ServerBase
{

	/**
	 * @param Server $server
	 * @param int $src_worker_id
	 * @param $message
	 */
	public function onPipeMessage(Server $server, int $src_worker_id, $message)
	{
		$prefix = '[' . date('Y-m-d H:i:s') . ']: ';
		$message = $prefix . print_r($message, true);

		$redis = BeReborn::$app->redis;

		/** @var BeReborn\Service\Http\Server $server */
		$server = \BeReborn::getApp('socket');
		if (!$server->enableWebSocket) {
			return;
		}

		$server = $server->getServer();
		$fds = $redis->sMembers('debug_list');

		foreach ($fds as $fd) {
			if (!$server->exist($fd)) {
				$redis->sRem('debug_list', $fd);
				continue;
			}
			$server->push($fd, $message);
		}
	}
}
