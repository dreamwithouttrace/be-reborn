<?php
/**
 * Created by PhpStorm.
 * User: whwyy
 * Date: 2018/6/27 0027
 * Time: 17:49
 */

namespace BeReborn\Database;


use BeReborn\Database\Orm\Select;
use BeReborn\Database\Traits\QueryTrait;

/**
 * Class Sql
 * @package BeReborn\Database
 */
class Sql
{
	
	use QueryTrait;
	
	/**
	 * @return string
	 * @throws \Exception
	 */
	public function getSql()
	{
		return (new Select())->getQuery($this);
	}
}
