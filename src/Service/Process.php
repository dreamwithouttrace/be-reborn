<?php


namespace BeReborn\Service;


/**
 * Class Process
 * @package BeReborn\Service
 */
abstract class Process extends \Swoole\Process
{


	/**
	 * Process constructor.
	 * @param null $pipe_type
	 * @param null $enable_coroutine
	 */
	public function __construct($pipe_type = null, $enable_coroutine = null)
	{
		parent::__construct([$this, 'onHandler'], false, $pipe_type, $enable_coroutine);
	}


	/**
	 * @param \Swoole\Process $process
	 * @return mixed
	 */
	abstract public function onHandler(\Swoole\Process $process);

}
