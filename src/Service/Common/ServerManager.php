<?php


namespace BeReborn\Service\Common;


use Exception;
use Swoole\WebSocket\Server;
use BeReborn\Service\Base\ServerBase;

/**
 * Class ServerManager
 * @package Yoc\server
 */
class ServerManager extends ServerBase
{

	/**
	 * @param Server $server
	 * @throws Exception
	 */
	public function onManagerStart(Server $server)
	{
		$this->debug('manager start.');
		\BeReborn::setProcessId($server->manager_pid);
		if (\BeReborn::getPlatform()->isLinux()) {
			swoole_set_process_name('Server Manager.');
		}
	}

	/**
	 * @param Server $server
	 * @throws Exception
	 */
	public function onManagerStop(Server $server)
	{
		$this->warning('manager stop.');
		$runPath = \BeReborn::getRuntimePath('workerIds');
		foreach (glob($runPath . '/*') as $item) {
			if (!file_exists($item)) {
				continue;
			}
			@unlink($item);
		}
	}


}
