<?php


namespace BeReborn\Service\Base;

use BeReborn\Base\BaseObject;

/**
 * Class ClientInfo
 * @package BeReborn\Service\Base
 * @property $server_socket
 * @property $server_port
 * @property $address
 * @property $port
 * @property $reactor_id
 * @property $server_fd
 * @property $fd
 * @property $remote_port
 * @property $remote_ip
 * @property $connect_time
 * @property $last_time
 */
class ClientInfo extends BaseObject
{

	protected $_serverInfo = [];


	/**
	 * @param $name
	 * @return mixed|null
	 */
	public function __get($name)
	{
		return $this->_serverInfo[$name] ?? null;
	}


	/**
	 * @param $name
	 * @param $value
	 */
	public function __set($name, $value)
	{
		$this->_serverInfo[$name] = $value;
	}

}
