<?php
/**
 * Created by PhpStorm.
 * User: whwyy
 * Date: 2018/4/8 0008
 * Time: 17:29
 */

namespace BeReborn\Http\Formatter;


/**
 * Interface IFormatter
 * @package BeReborn\Http\Formatter
 */
interface IFormatter
{

	/**
	 * @param $context
	 * @return static
	 */
	public function send($context);

	public function getData();

	public function clear();
}
