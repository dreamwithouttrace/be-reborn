<?php


namespace BeReborn\Service\Common;


use BeReborn\Http\Context;
use BeReborn\Http\HttpHeaders;
use BeReborn\Http\HttpParams;
use BeReborn\Http\Request;
use BeReborn\Service\Base\ClientInfo;
use BeReborn\Service\Base\ServerBase;

/**
 * Class Port
 * @package BeReborn\Service\Common
 */
abstract class Port extends ServerBase
{

	/**
	 * @param $data
	 * @param $fd
	 * @param $server
	 * @param $method
	 * @throws \Exception
	 */
	public function newRequestInstance($data, $fd, $server, $method = 'receive')
	{
		$req = new Request();
		$req->startTime = microtime(true);
		$req->params = new HttpParams($data['body'] ?? [], [], []);

		$clientInfo['class'] = ClientInfo::class;
		$req->clientInfo = \BeReborn::createObject(array_merge($clientInfo, $server ?? []));

		$req->headers = new HttpHeaders(array_merge([
			'request_method' => $method,
			'request_uri'    => $server['server_port'] . '/' . $data['path']
		], $data['header'] ?? []));
		$req->setFd($fd);
		$req->parseUri();

		Context::setRequest($req);
	}
}
