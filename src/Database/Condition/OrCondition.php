<?php


namespace BeReborn\Database\Condition;



/**
 * Class OrCondition
 * @package BeReborn\Database\Condition
 */
class OrCondition extends Condition
{

	/**
	 * @return string
	 */
	public function builder()
	{
		return 'OR ' . $this->resolve($this->column, $this->value, $this->opera);
	}

}
