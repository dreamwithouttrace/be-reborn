<?php


namespace BeReborn\Console;

/**
 * Interface CommandInterface
 * @package BeReborn\Console
 */
interface CommandInterface
{

	public function handler(Dtl $dtl);

}
