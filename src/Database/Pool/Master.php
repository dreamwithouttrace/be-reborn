<?php


namespace BeReborn\Database\Pool;

/**
 * Class MasterConnect
 * @package BeReborn\pool
 *
 * \Db::table()
 */
class Master extends DbConnection
{

	public $host = '';
	public $username = '';
	public $password = '';


	public $masterConfig = [];

	/**
	 * @return mixed|void
	 */
	public function getConnectConfig()
	{
		$config = $this->masterConfig;
		$config[] = [
			'cds' => $this->host,
			'username' => $this->username,
			'password' => $this->password
		];
		return $config[array_rand($config)];
	}

}
