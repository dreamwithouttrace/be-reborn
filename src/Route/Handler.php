<?php


namespace BeReborn\Route;


use BeReborn\Base\BaseObject;
use BeReborn\Exception\ComponentException;

/**
 * Class TcpListen
 * @package BeReborn\Route
 */
class Handler extends BaseObject
{

	protected $router;

	/**
	 * Listen constructor.
	 * @throws ComponentException
	 */
	public function __construct()
	{
		$this->router = \BeReborn::$app->getRouter();

		parent::__construct([]);
	}

	/**
	 * @param $config
	 * @param $handler
	 */
	public function group($config, $handler)
	{
		$this->router->group($config, $handler, $this);
	}


	/**
	 * @param $route
	 * @param $handler
	 * @return Handler
	 */
	public function handler($route, $handler)
	{
		return $this->router->addRoute($route, $handler, 'receive');
	}

}
