<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019-03-22
 * Time: 16:00
 */

namespace BeReborn\Task;


use BeReborn\Base\Component;

/**
 * Class Task
 * @package BeReborn\Task
 */
abstract class Task extends Component implements InterfaceTask
{

	public $param;

	/**
	 * Task constructor.
	 * @param array $data
	 */
	public function __construct($data)
	{
		$this->param = $data;
		parent::__construct([]);
	}

	/**
	 * @return mixed
	 */
	public function getParams()
	{
		return $this->param;
	}

	/**
	 * @param $name
	 * @return mixed|null
	 * @throws \Exception
	 */
	public function __get($name)
	{
		if (isset($this->param[$name])) {
			return $this->param[$name];
		}
		return null;
	}
}
