<?php


namespace BeReborn\Canvas;

/**
 * Interface Canvas
 * @package BeReborn\Canvas
 */
interface Canvas
{

	public function Draw();

	public function getChildes();

}
