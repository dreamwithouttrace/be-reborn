# 瞎写的

#### 介紹
瞎写的，自己用的

#### 架構
瞎几吧乱抄的, 依赖以下扩展
```text
phpredis
swoole
inotify
```

#### 安裝教程
```text
新建 $dir/composer.json  
新建 $dir/app/controller  
新建 $dir/app/model  
新建 $dir/app/middleware  
新建 $dir/commands  
新建 $dir/components  
新建 $dir/config  
新建 $dir/routes 
``` 

添加composer 内容  
```json
{
	"autoload": {
		"psr-4": {
			"App\\": "app"
		},
		"files": [
		]
	},
	"require": {
		"php": ">= 7.1",
	},
	"require-dev": {
		"forset/forset": "dev-master",
		"swoole/ide-helper": "@dev"
	}
}

```


```bash
执行 composer update
```

#### 使用說明

新建 $dir/routes/web.php
```php
<?php

use BeReborn\Route\Router;

/** @var Router $router */
$router->get('index', 'SiteController@index');
$router->post('index', 'SiteController@index');
$router->any('index', 'SiteController@index');
$router->delete('index', 'SiteController@index');
$router->put('index', 'SiteController@index');

$options = [
    'prefix' => '', //前缀
    'namespace' => '', //Controller使用的命名空间如  namespace='server'则访问 app\controller\server\TestController
    'filter' => [   // 过滤请求用的, 参数效验同 model写法
    	 'grant' => [] ,    //权限效验回调函数
    	 'header' => [      //效验请求头所需数据
    	 	[['token', 'user', 'time', 'source'], 'required'],
    	 	[['token', 'source'], 'string'],
    	 	[['user', 'time'], 'int', 'maxLength' => 32],
         ],
    	 'body' => [      //效验请求体所需数据
    	 	[['token', 'user', 'time', 'source'], 'required'],
    	 	[['token', 'source'], 'string'],
    	 	[['user', 'time'], 'int', 'maxLength' => 32],
         ]
     ],     
    'middleware' => '',  // 中间件	
    'options' => '',  // ajax跨域请求处理	
];

$router->group($options, function (\BeReborn\Route\Router $router){
    $router->get('index', 'SiteController@index');
    $router->post('index', 'SiteController@index');
    $router->any('index', 'SiteController@index');
    $router->delete('index', 'SiteController@index');
    $router->put('index', 'SiteController@index');
});

```

新建 $dir/execfile并添加内容
```php
<?php
error_reporting(E_ALL & ~E_NOTICE);

define('APP_PATH', __DIR__);
define('DISPLAY_ERRORS', TRUE);
define('DEBUG', TRUE);
define('DB_EMPTY', 3001);
define('DB_ERROR', 3002);
define('PARAM_NOT_EXISTS', 4001);
define('PARAM_EMPTY', 4004);

//ini_set('memory_limit','8192M');

use BeReborn\Web\Application;

require_once __DIR__ . '/vendor/autoload.php';
$config = require_once __DIR__ . '/config/configure.php';
$application = new Application($config);
$snooze = $application->getSwooleServer($argv);
if (!$snooze) {
	return;
}
$snooze->start();

```

添加配置项内容 $dir/config/configure.php
```php
<?php

```

```bash
启动  php $dir/execfile 或 php $dir/execfile start  
重启  php $dir/execfile restart   
停止  php $dir/execfile stop 
```  

#Command
注册命令  
```php
<?php
use BeReborn\Console\Command;
use BeReborn\Console\Dtl;

class exranpk extends Command{
	
	public $command = 'exmple:test';
	
	public $description = '任务描述';
	
	public $dataFile = '/usr/local/config.json';
	
	public $dataType = 'json';
	
	public function handler(Dtl $dtl)
	{
		
	}
	
}
```


使用  
```bash
php artisan command_name --key=o --key=b --key=v
```

#### 參與貢獻

1. Fork 本倉庫
2. 新建 Feat_xxx 分支
3. 提交代碼
4. 新建 Pull Request


#### 碼雲特技

1. 使用 Readme\_XXX.md 來支持不同的語言，例如 Readme\_en.md, Readme\_zh.md
2. 碼雲官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 妳可以 [https://gitee.com/explore](https://gitee.com/explore) 這個地址來了解碼雲上的優秀開源項目
4. [GVP](https://gitee.com/gvp) 全稱是碼雲最有價值開源項目，是碼雲綜合評定出的優秀開源項目
5. 碼雲官方提供的使用手冊 [https://gitee.com/help](https://gitee.com/help)
6. 碼雲封面人物是壹檔用來展示碼雲會員風采的欄目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
